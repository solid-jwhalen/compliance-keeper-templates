if( $('.table-view_tabs').length ){

  var tabCount = $('.table-view_tabs-item').length,
      tabWidth = "calc(" + ( 100 / tabCount ) + "% - 3rem)",
      containerWidth = 100 * tabCount + "%",
      contentWidth = (100 / tabCount ) + "%";

  $('.table-view_container').css('width', containerWidth);

  $('.table-view_content_wrapper').css('width', contentWidth);

}

$('.table-view_tabs-item').click(function(e){
  e.preventDefault();

  var tabId = $(this).attr('class').split(' ').pop(),
      tabCount = $('.table-view_tabs-item').length,
      tabShift = (( 100 / tabCount ) * tabId) * -1 + "%";

    $('.table-view_tabs-item').removeClass('active');

    $(this).addClass('active');

    $('.table-view_container').css('transform', 'translateX(' + tabShift + ')');

    console.log(tabShift);

});

$('.table-view_content_tab-title').click(function(e){
  e.preventDefault();

  var self = $(this).parent(),
      maxHeight = $(this).parent().outerHeight();

  $(this).parent().toggleClass('collapsed');

  if( $(this).parent().hasClass('collapsed') ){
    self.css( 'max-height', maxHeight );
  }

});
