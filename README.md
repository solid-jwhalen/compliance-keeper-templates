# ocl-ck

### Build
- `npm install`
- `bower install` - install with `npm install -g bower` if needed
- `npm run dev` or `gulp` (if you have gulp installed globally) in the terminal window, and you're all set up!

### Develop
- We use the twig templating engine to generate our static html pages
- Data is loaded into the views from the .json file in each page

## Provision
- `bin/provision` - configures nginx on staging server to serve project's public dir 
  - Requires `ansible`. If needed, install with:
    - `sudo easy_install pip`
    - `sudo pip install ansible`

## Deploy
- init local git repo and push to origin (gitlab repo you created before scaffolding)
- ensure the gitlab repo has the wordpress.thinksolid.com deployment key enabled (ask Peter)
- `git push -u origin develop`
  - if deployment key enabled correctly, gitlab will automatically push `develop` to staging
  - gulp build process is automatically run on staging server after each update
- project available at <project_slug>.thinksolid.com

## Project Overview
- `src`: Folder containing all html, js, and scss
  - `components`: Pods of markup, with associated styles and javascript
    - `<component-name>`: folder for each component pod
      - `.html`
      - `.js`
      - `.scss`
      - If component is only single file, need not be nested in a folder, but go ahead.
  - `img`: Folder for all uncompressed images.
  - `js`: Folder for all JavaScript not associated with a component
  - `sass`: Folder for all SASS not associated with a component
  - `pages`: like components, but for pages that extend the layout html files
    - `<page-name>`: dir for each page, each page here will be built to a page in the public dir
      - `.twig`: markup for the page written in twig
      - `.js`: any js for the page
      - `.scss`: any styles for the page
      - `.json`: any data to be templated into the twig template
  - `vendor`: Folder containing all vendor libraries.
    - install it with `bower`, e.g. `bower install --save <package-name>`
    - For js, add package's main file to the src list of the vendor-js task in `gulpfile.js`
    - For scss, import from `application.scss`
  - `application.js`: Global js not associated with any component
  - `application.scss`: Entry point for sass globbing
  - `layout.twig`: html layout, extended by pages
  - `layout.json`: global data for layout and global components (header, footer), can be overwritten by page specific data
- `public`: Output of gulp build process
  - `css`: Compiled SASS.
  - `js`: Compiled JS and libraries.
  - `img`: Compiled images.
  - `index.html`: The default landing page for the project.
  - `t1.html`, etc...
  
## Includes
- [Autoprefixer](https://www.npmjs.com/package/gulp-autoprefixer): Automatically adds vendor prefixing for CSS components when needed.
- [SASS](https://www.npmjs.com/package/gulp-sass): Compiles all your SCSS files into usable CSS.
- [Concat](https://www.npmjs.com/package/gulp-concat): Combines small files into one larger file for deployment.
- [Flatten](https://www.npmjs.com/package/gulp-flatten): Makes messing with file directorys and paths a little simpler.
- [Imagemin](https://www.npmjs.com/package/gulp-imagemin): Compresses image files so they load faster, and dont take up so much space on the server.
- [CSS Globbing](https://www.npmjs.com/package/gulp-css-globbing): Allows you to reference directories in your Application.scss file, preventing you from having to include every new scss file.
- [CMQ](https://www.npmjs.com/package/gulp-combine-media-queries): Combine matching media queries into one media query definition.
- [Gulp Newer](https://www.npmjs.com/package/gulp-newer): Speeds up processing by only reprocessing source files that are newer than their compiled counterparts.
- [Gulp Uglify](https://www.npmjs.com/package/gulp-uglify) - Compresses your JS files for smaller file size, and faster loading.
- [Connect Livereload](https://www.npmjs.com/package/connect-livereload):An implementation of the LiveReload server in Node.js. It's an alternative to the graphical http://livereload.com/ application, which monitors files for changes and reloads your web browser.
- [Gulp Connect](https://www.npmjs.com/package/gulp-connect): Sets up a server compatible with LiveReload, allowing you to view your compiled files as you work on them without the need to mash CMD+R.
- [Gulp Newer](https://www.npmjs.com/package/gulp-newer): Speeds up livereload by only having it process new files.
- [Gulp Sourcemaps](https://www.npmjs.com/package/gulp-sourcemaps): Allows you to see where a line of CSS or JS originated when viewing things in the inspector.
- [Gulp Open](https://www.npmjs.com/package/gulp-open): Opens up the default dev view of "localhost:8080"
- [Gulp Data](https://www.npmjs.com/package/gulp-data): This package allows us to load data into our pages from a json file
- [Gulp Twig](https://www.npmjs.com/package/gulp-twig): This package allows us to modularize our HTML, and use the twig templating engine.

## Options
- Locally running WebServer
  - If you'd like to perform any testing on a device other than the one you're running gulp on:
    1. `npm run build && cd public && python -m SimpleHTTPServer`
    4. Open up on your mobile device, or any other device connected to the same wifi network the computer you're running gulp is being on.
    5. Enter your IP Address, along with the port number you see in the terminal as the address.
    6. Have fun!

### Notes
- Use [`nvm`](https://github.com/creationix/nvm) to manage your node versions.
- Do not `sudo` any npm commands
