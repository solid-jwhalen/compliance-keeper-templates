'use strict';

module.exports = {

    apps : [ ],

    /**
     * Deployment section
     * http://pm2.keymetrics.io/docs/usage/deployment/
     */
    deploy : {
        staging : {
            user : "ubuntu",
            host : "wordpress.thinksolid.com",
            ref : "origin/develop",
            repo : "",
            path : "/var/www/vhosts/ocl_ck.thinksolid.com",
            "pre-deploy-local": "pm2 deploy deploy/ocl_ck.config.js staging setup || echo 'already setup'",
            "post-deploy" : "/var/www/vhosts/ocl_ck.thinksolid.com/current/deploy/postDeploy && exit $?"
        }
    }
};
